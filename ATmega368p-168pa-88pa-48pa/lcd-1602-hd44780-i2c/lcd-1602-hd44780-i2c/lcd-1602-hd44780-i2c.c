#define F_CPU 1000000UL

#include "../../_avr8_include/avr/io.h"
#include "../../_avr8_include/util/delay.h"
#include "lcd1602.h"


int main(void) {
  LCD1602Init();

  while (0X01) {
    LCD1602Clear();
    LCD1602GoToXY(0, 0);
    LCD1602SendString("oi, Neto!");
	
    _delay_ms(500);
  }
}
