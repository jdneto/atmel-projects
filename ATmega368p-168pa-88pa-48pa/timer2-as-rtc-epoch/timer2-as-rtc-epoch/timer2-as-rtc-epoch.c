#define F_CPU 1000000UL

#include "../../_avr8_include/avr/io.h"
#include "../../_avr8_include/avr/interrupt.h"

volatile uint32_t epoch = 0; //- may be changed to actual epoch time => epochconverter.com.

int main (void) {
	DDRB = (1 << DDB5) | (1 << DDB4) | (1 << DDB3) | (1 << DDB2) | (1 << DDB1) | (1 << DDB0); //- set all PORTB ports as OUTPUT.
	DDRC = (1 << DDC6) | (1 << DDC5) | (1 << DDC4) | (1 << DDC3) | (1 << DDC2) | (1 << DDC1) | (1 << DDC0); //- set all PORTC ports as OUTPUT.
	DDRD = (1 << DDD7) | (1 << DDD6) | (1 << DDD5) | (1 << DDD4) | (1 << DDD3) | (1 << DDD2) | (1 << DDD1) | (1 << DDD0); //- set all PORTD ports as OUTPUT.

	TCCR2B |= (1 << CS22) | (1 << CS20); //- set TIMER2 prescaller CLK/128 => 32.768kHz / 128 => 256.
	ASSR  = (1 << AS2); //- enable TIMER2 asynchronous mode.
	TCNT2 = 0; //- set TIMER2 counter to zero.
	
	while (ASSR & ((1 << TCN2UB) | (1 << TCR2BUB))); //- wait registers update.
	
	TIFR2  = (1 << TOV2); //- manually clears TIMER2 interrupt flag.
	TIMSK2  = (1 << TOIE2); //- enable TIMER2 overflow interrupt.

	SMCR |= (1 << SM1) | (1 << SM0); //- set power-save sleep mode.
	
	__asm__  __volatile__("sei"); //- enable global interrupts.
	
	while (0x01) {
		MCUCR = (1 << BODS) | (1 << BODSE); //- prepare brown-out detection...
		MCUCR = (1 << BODS) | (MCUCR & ~(1 << BODSE)); //- ... for sleep.

		SMCR |= (1 << SE); //- enable sleep.
		__asm__  __volatile__("sleep"); //- put the microcontroller to sleep.
		SMCR &= ~(1 << SE); //- disable sleep.
	}
}

ISR(TIMER2_OVF_vect) { //- called every time TIMER2 overflows.
  epoch++;
}