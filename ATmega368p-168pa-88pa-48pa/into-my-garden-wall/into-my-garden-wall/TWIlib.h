/*
 * TWIlib.h
 *
 * Created: 6/01/2014 10:38:42 PM
 *  Author: Chris Herring
 */ 


#ifndef TWILIB_H_

#define TWILIB_H_
#define TWI_FREQ 50000 //- TWI bit rate.
#define TWI_STATUS (TWSR & 0xF8) //- get TWI status.
#define TXMAXBUFLEN 20 //- transmit buffer length.
#define RXMAXBUFLEN 20 //- receive buffer length.

uint8_t TWITransmitBuffer[TXMAXBUFLEN]; //- global transmit buffer.
volatile uint8_t TWIReceiveBuffer[RXMAXBUFLEN]; //- global receive buffer

volatile uint16_t TXBuffIndex; //- index of the transmit buffer; is volatile, can change at any time.
uint16_t RXBuffIndex; //- current index in the receive buffer.

uint16_t TXBuffLen; //- the total length of the transmit buffer
uint16_t RXBuffLen; //- the total number of bytes to read (should be less than RXMAXBUFFLEN).

uint8_t TWIStatusCheck;

typedef enum {
  Ready,
  Initializing,
  RepeatedStartSent,
  MasterTransmitter,
  MasterReceiver,
  SlaceTransmitter,
  SlaveReciever
} TWIMode;

typedef struct TWIInfoStruct{
  TWIMode mode;
  uint8_t errorCode;
  uint8_t repStart; 
} TWIInfoStruct;
TWIInfoStruct TWIInfo;

#define TWI_START_SENT 0x08 //- start sent.
#define TWI_REP_START_SENT 0x10 //- repeated start sent.

#define TWI_MT_SLAW_ACK 0x18 //- SLA+W sent and ACK received.
#define TWI_MT_SLAW_NACK 0x20 //- SLA+W sent and NACK received.
#define TWI_MT_DATA_ACK 0x28 //- data   sent and ACK received.
#define TWI_MT_DATA_NACK 0x30 //- data sent and NACK received.

#define TWI_MR_SLAR_ACK 0x40 //- SLA+R sent, ACK received.
#define TWI_MR_SLAR_NACK 0x48 //- SLA+R sent, NACK received.
#define TWI_MR_DATA_ACK 0x50 //- data received, ACK returned.
#define TWI_MR_DATA_NACK 0x58 //- data received, NACK returned.

#define TWI_LOST_ARBIT 0x38 //- arbitration has been lost.
#define TWI_NO_RELEVANT_INFO 0xF8 //- no relevant information available.
#define TWI_ILLEGAL_START_STOP 0x00 //- illegal START or STOP condition has been detected.
#define TWI_SUCCESS 0xFF //- successful transfer, this state is impossible from TWSR as bit2 is 0 and read only.

#define TWISendStart() (TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN) | (1 << TWIE)) //- send the START signal, enable interrupts and TWI, clear TWINT flag to resume transfer.
#define TWISendStop() (TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN) | (1 << TWIE)) //- send the STOP signal, enable interrupts and TWI, clear TWINT flag.
#define TWISendTransmit() (TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWIE)) //- used to resume a transfer, clear TWINT and ensure that TWI and interrupts are enabled.
#define TWISendACK() (TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWIE) | (1 << TWEA)) //- for MR mode: resume a transfer, ensure that TWI and interrupts are enabled and respond with an ACK if the device is addressed as a slave or after it receives a byte.
#define TWISendNACK() (TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWIE)) //- for MR mode: resume a transfer, ensure that TWI and interrupts are enabled but DO NOT respond with an ACK if the device is addressed as a slave or after it receives a byte.

void TWIInitialize(void);
void TWIClose(void);
uint8_t TWITransmitData(void* const TXdata, uint8_t dataLen, uint8_t repStart);
uint8_t TWIReadData(uint8_t TWIaddr, uint8_t bytesToRead, uint8_t repStart);
void TWIStatusChecker(void);

#endif