#define F_CPU 1000000UL
#define F_I2C 50000UL
#define LCD_ADDRESS 0x3F

#include "../../_avr8_include/avr/io.h"
#include "../../_avr8_include/avr/interrupt.h"
#include "../../_avr8_include/util/delay.h"

static struct {
	uint8_t LEDPin;
} LCD;

volatile uint8_t increaseTime = 0x00; //- control the increase of seconds.
volatile uint8_t b0Press = 0x00, b1Press = 0x00; //- control the buttons press.

void timeToChar(uint8_t timeValue, uint8_t* string, uint8_t strPosition);
void nibble(uint8_t nb);
void I2CSendByte(uint8_t byte);
void I2CSendPacket(uint8_t value, uint8_t address);
void LCDStart(void);
void LCDSendByte(uint8_t byte, uint8_t systemCommand);
void LCDGoTo(uint8_t x, uint8_t y);

int main (void) {
  uint8_t hour = 5, minute = 59, second = 55;
  
  uint8_t alarm1 = 6, alarm2 = 18; //- the alarms (6h, 18h).
  uint8_t actionInProgress = 0x00; //- control the alarm action execution.
  uint8_t actionTimer = 0x00; //- control the action time.
  
  uint8_t allowSleep = 0x01; //- allow (or not) the microcontroller to sleep.

  uint8_t systemON = 0x00;
  uint8_t clockMode = 0x00;
  uint8_t clockTimer = 0x00, previousClockTimer = clockTimer;
  uint8_t blinkToAdjust = 0x00;

  //- PORT SETUP -------------------------------------------------------------
	
  DDRB = 0x3F; //- [PORTB5..PORTB0] as OUTPUT.
	DDRC = 0x7F; //- [PORTC6..PORTC0] as OUTPUT.
	DDRD = 0xF3; //- [PORTD7..PORTD4], PORTD1, PORTD0 as OUTPUT; PORTD2(INT0), PORTD3(INT1) as INPUT.

  PORTB = 0x00;
  PORTC = 0x00;
  PORTD = 0x00;

  //- CLOCK SETUP ------------------------------------------------------------

	TCCR2B |= (1 << CS22) | (1 << CS20); //- set TIMER2 prescaller CLK/128 => 32.768kHz / 128 => 256.
	ASSR  = (1 << AS2); //- enable TIMER2 asynchronous mode.
	TCNT2 = 0x00; //- set TIMER2 counter to zero.
	
	while (ASSR & ((1 << TCN2UB) | (1 << TCR2BUB))); //- wait registers update.
	
	TIFR2  = (1 << TOV2); //- manually clears TIMER2 interrupt flag.
	TIMSK2  = (1 << TOIE2); //- enable TIMER2 overflow interrupt.

	//- SLEEP MODE SETUP -------------------------------------------------------
  
  SMCR |= (1 << SM1) | (1 << SM0); //- set power-save sleep mode.

  //- EXTERNAL INTERRUPT SETUP -----------------------------------------------

  EICRA |= (1 << ISC11) | (1 << ISC10) | (1 << ISC01) | (1 << ISC00); //- INT0 requested on RISING edge change in PORTD2; INT1 requested on RISING edge change in PORTD3.
  EIMSK |= (1 << INT0); //- enable INT0.
	
	//- GLOBAL INTERRUPT SETUP -------------------------------------------------
  
  __asm__  __volatile__("sei"); //- enable global interrupts.

 	while (0x01) {
    //- SLEEP CONTROL --------------------------------------------------------
    
    if (allowSleep) {
      MCUCR = (1 << BODS) | (1 << BODSE); //- prepare brown-out detection...
      MCUCR = (1 << BODS) | (MCUCR & ~(1 << BODSE)); //- ... for sleep.

      SMCR |= (1 << SE); //- enable sleep.
      __asm__  __volatile__("sleep"); //- put the microcontroller to sleep.
      SMCR &= ~(1 << SE); //- disable sleep.
    }

    allowSleep = 0x01;

		//- CLOCK CONTROL --------------------------------------------------------
    
    if (increaseTime) {
      increaseTime--;  
      allowSleep &= 0x00;

      if (++second > 59) { //- from here...
        second = 0;

        if (++minute > 59) {
          minute = 0;

          if (++hour > 23) {
            hour = 0;
          }
        }
      } //- ... to here, the time values control.

      if (clockTimer) {
        previousClockTimer = clockTimer;
        clockTimer--;

        if (clockMode > 0x01) {
          blinkToAdjust = !blinkToAdjust;
        }
      }

      if (actionTimer) {
        actionTimer--;
      }
    }

    //- ALARM/ACTION CONTROL -------------------------------------------------

    if (hour == alarm1 || hour == alarm2) {
      if (!actionInProgress) {
        if (clockMode <= 0x01) {
          PORTD |= (1 << PORTD7);

          actionInProgress = 0x01;
          actionTimer = 0x3C;
        }
      } else if (!actionTimer) {
        PORTD &= ~(1 << PORTD7);
      }
    } else {
      if (actionInProgress) {
        actionInProgress = 0x00;
      }
    }

    //- BUTTON 0 CONTROL -----------------------------------------------------

    if (b0Press) {
      b0Press--;

      if (!(systemON++)) {
        allowSleep &= 0x00;
        PORTB |= (1 << PORTB1);
        EIMSK |= (1 << INT1);
        LCDStart();
      }

      if (++clockMode > 0x03) {
        clockMode = 0x01;
      }

      if (clockMode > 0x01) {
        clockTimer = 0x05;
      } else {
        clockTimer = 0x3C;
      }
    }

    //- BUTTON 1 CONTROL -----------------------------------------------------

    if (b1Press) {
      // if (!(PORTD & (1 << PORTD3))) {
        b1Press--;
      // }
      
      if (clockMode == 0x02) {
        if (++hour > 23) {
          hour = 0;
        }
      } else if (clockMode == 0x03) {
        if (++minute > 59) {
          minute = 0;
        }
      }
      
      clockTimer = 0x05;
    }

    //- LCD CONTROL ----------------------------------------------------------

    if (systemON && ((previousClockTimer > clockTimer) || clockMode > 0x01)) {
      uint8_t timeString[0x08];
      uint8_t startX = 0x08;

      timeToChar(hour, timeString, 0x00);
      timeString[0x02] = 58; //- 58 => ":".
      timeToChar(minute, timeString, 0x03);
      timeString[0x05] = 58; //- 58 => ":".
      timeToChar(second, timeString, 0x06);
      
      if (clockMode == 0x01) {
        LCDGoTo(startX, 0x01);
        for(uint8_t index = 0x00; index < 0x06; index++) {
          LCDSendByte(timeString[index], 0x00);
        }
      } else if (clockMode == 0x02 || clockMode == 0x03) {
        if (clockMode == 0x02) {
          LCDGoTo(startX, 0x01);
          if (!blinkToAdjust) {
            for(uint8_t index = 0x00; index <= 0x01; index++) {
              LCDSendByte(timeString[index], 0x00);
            }
          }
        } else {
          LCDGoTo(startX + 0x03, 0x01);
          if (!blinkToAdjust) {  
            for(uint8_t index = 0x03; index <= 0x04; index++) {
              LCDSendByte(timeString[index], 0x00);
            }
          }
        }
        
        if (blinkToAdjust) {
          LCDSendByte(' ', 0x00); LCDSendByte(' ', 0x00);
        }        
      }

      LCDGoTo(startX + 0x06, 0x01);
      for(uint8_t index = 0x06; index < 0x08; index++) {
        LCDSendByte(timeString[index], 0x00);
      }
    }

    //- TIMER CONTROL --------------------------------------------------------

    if (systemON && !clockTimer) {
      if (clockMode > 0x01) {
        clockMode = 0x01;
        clockTimer = 0x32;
      } else {
        clockMode = systemON = 0x00;
        
        TWCR &= ~(1 << TWEN);
        PORTB &= ~(1 << PORTB1);
        EIMSK &= ~(1 << INT1);
      }
    }
	}
}

//- INTERRUPT FUNCTIONS ----------------------------------------------------

ISR(TIMER2_OVF_vect) { //- called every time TIMER2 overflows.
  increaseTime++;
}

ISR(INT0_vect) { //- called on PORTD2 RISING edge.
  b0Press = 0x01; //- button 0 was pressed.
}

ISR(INT1_vect) { //- called on PORTD3 RISING edge
  b1Press = 0x01; //- button 1 was pressed.
}

//- HELPER FUNCTIONS -------------------------------------------------------

void timeToChar(uint8_t timeValue, uint8_t* string, uint8_t strPosition) {
  //- [48..57] => ["0".."9"].
  if (timeValue < 10) {
    string[strPosition] = 48;
    string[strPosition + 1] = timeValue + 48;
  } else {
    string[strPosition] = (timeValue / 10) + 48; //- (timeValue / 10) => eg.: (23 / 10) = 2.
    string[strPosition + 1] = (timeValue % 10) + 48; //- (timeValue % 10) => eg.: (23 % 10) => 3.
  }
}

void nibble(uint8_t nb) {
	I2CSendPacket(LCD.LEDPin |= 0x04, (LCD_ADDRESS << 1));
	
  _delay_us(0x32);
	
  I2CSendPacket(LCD.LEDPin | (nb << 0x04), (LCD_ADDRESS << 1));
	I2CSendPacket(LCD.LEDPin &= ~0x04, (LCD_ADDRESS << 1));
		
  _delay_us(0x32);
}

//- I2C FUNCTIONS ----------------------------------------------------------

void I2CSendByte(uint8_t byte) {
	TWDR = byte;
	TWCR = (1 << TWINT) | (1 << TWEN);
	while (!(TWCR & (1 << TWINT)));
}

void I2CSendPacket(uint8_t value, uint8_t address) {
  TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
	while (!(TWCR & (1 << TWINT)));

	I2CSendByte(address);
	I2CSendByte(value);
	
  TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);
}

//- LCD FUNCTIONS ----------------------------------------------------------

void LCDStart(void) {
  TWSR = 0x00; //- set TWI pre-scalers (no pre-scaling).
  TWBR = ((F_CPU / F_I2C) - 16) / 2; //- set TWI bit rate.
  TWCR |= (1 << TWEN); //- enable TWI.

  LCD.LEDPin = 0x00;
  	
  _delay_ms(0x0F);
	nibble(0x03);
	_delay_ms(0x04);
	nibble(0x03);
	_delay_us(0x64);
	nibble(0x03);
	_delay_ms(0x01);
	nibble(0x02);
	_delay_ms(0x01);
  
  LCDSendByte(0x28, 0x01);
	_delay_ms(0x01);
	LCDSendByte(0x0C, 0x01);
	_delay_ms(0x01);
	LCDSendByte(0x06, 0x01);
	_delay_ms(0x01);
  
  I2CSendPacket(LCD.LEDPin |= 0x08, (LCD_ADDRESS << 1));
	I2CSendPacket(LCD.LEDPin &=~ 0x02, (LCD_ADDRESS << 1));
}

void LCDSendByte(uint8_t byte, uint8_t systemCommand) {
	if (systemCommand) {
		I2CSendPacket(LCD.LEDPin &=~ 0x01, (LCD_ADDRESS << 1));
  } else {
		I2CSendPacket(LCD.LEDPin |= 0x01, (LCD_ADDRESS << 1));
  }
	
  nibble(byte >> 0x04);
	nibble(byte);
}

void LCDGoTo(uint8_t x, uint8_t y) {
  LCDSendByte(((0x40 * y + x) | 0x80), 0x01);
}
