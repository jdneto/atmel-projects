/*
 * TWIlib.c
 *
 *  Created: 6/01/2014 10:41:33 PM
 *  Author: Chris Herring
 */ 

#include "../../_avr8_include/avr/io.h"
#include "../../_avr8_include/avr/interrupt.h"
#include "../../_avr8_include/util/delay.h"
#include "TWIlib.h"

void TWIInitialize(void) {
  TWIStatusCheck = 0x00;

  TWIInfo.mode = Ready;
  TWIInfo.errorCode = 0xFF;
  TWIInfo.repStart = 0x00;

  TWSR = 0x00; //- set pre-scalers (no pre-scaling).
  TWBR = ((F_CPU / TWI_FREQ) - 16) / 2; //- set bit rate.
  TWCR = (1 << TWIE) | (1 << TWEN); //- enable TWI and interrupt.
}

void TWIClose(void) {
  TWCR &= ~((1 << TWIE) | (1 << TWEN)); //- disable TWI and interrupt.
}

uint8_t TWITransmitData(void* const TXdata, uint8_t dataLen, uint8_t repStart) {
  if (dataLen <= TXMAXBUFLEN) {
    while (!((TWIInfo.mode == Ready) | (TWIInfo.mode == RepeatedStartSent))) {
      _delay_us(1);
    } //- wait until ready.

    TWIInfo.repStart = repStart; //- set repeated start mode.
    
    uint8_t *data = (uint8_t *)TXdata; //- copy data...
    for (uint16_t index = 0x00; index < dataLen; index++) {
      TWITransmitBuffer[index] = data[index];
    } //- ... into the transmit buffer

    TXBuffLen = dataLen; //- copy transmit info...
    TXBuffIndex = 0x00; //- ... to global variables.
    
    if (TWIInfo.mode == RepeatedStartSent) { //- if a repeated start has been sent, then devices are already listening for an address and another start does not need to be sent. 
      TWIInfo.mode = Initializing;
      TWDR = TWITransmitBuffer[TXBuffIndex++]; //- load data to transmit buffer.
      TWISendTransmit(); //- send the data.
    } else { //- otherwise, just send the normal start signal to begin transmission.
      TWIInfo.mode = Initializing;
      TWISendStart();
    }    
  } else {
    return 0x00;
  }
  
  return 0x01;
}

uint8_t TWIReadData(uint8_t TWIaddr, uint8_t bytesToRead, uint8_t repStart) {
  if (bytesToRead < RXMAXBUFLEN) { //- check if number of bytes to read can fit in the RXbuffer.
    RXBuffIndex = 0x00; //- reset buffer index.
    RXBuffLen = bytesToRead; //- set RXBuffLen to the number of bytes to read.

    uint8_t TXdata[0x01]; //- create the one value array for the address to be transmitted.
    TXdata[0x00] = (TWIaddr << 1) | 0x01; //- shift the address and AND a 1 into the read write bit (set to write mode).
    TWITransmitData(TXdata, 1, repStart); //- use the TWITransmitData function to initialize the transfer and address the slave.
  } else {
    return 0x00;
  }

  return 0x01;
}

void TWIStatusChecker(void) {
  TWIStatusCheck = 0x00;

  switch (TWI_STATUS) {
    //- ----\/ ---- MASTER TRANSMITTER OR WRITING ADDRESS ----\/ ----  //

    case TWI_MT_SLAW_ACK: //- SLA+W transmitted and ACK received.
      TWIInfo.mode = MasterTransmitter; //- set mode to Master Transmitter
    case TWI_START_SENT: //- start condition has been transmitted.
    case TWI_MT_DATA_ACK: //- data byte has been transmitted, ACK received.
      if (TXBuffIndex < TXBuffLen) { //- if there is more data to send...
        TWDR = TWITransmitBuffer[TXBuffIndex++]; //- ... load data to transmit buffer...
        TWIInfo.errorCode = TWI_NO_RELEVANT_INFO;
        TWISendTransmit(); //- ... send the data.
      } else if (TWIInfo.repStart) { //- this transmission is complete however do not release bus yet.
        TWIInfo.errorCode = 0xFF;
        TWISendStart();
      } else { //- all transmissions are complete, exit.
        TWIInfo.mode = Ready;
        TWIInfo.errorCode = 0xFF;
        TWISendStop();
      }

      break;
    
    //- ----\/ ---- MASTER RECEIVER ----\/ ----  //
    
    case TWI_MR_SLAR_ACK: //- SLA+R has been transmitted, ACK has been received.
      TWIInfo.mode = MasterReceiver; //- switch to Master Receiver mode.
      if (RXBuffIndex < RXBuffLen - 0x01) { //- if there is more than one byte to be read, receive data byte and return an ACK.
        TWIInfo.errorCode = TWI_NO_RELEVANT_INFO;
        TWISendACK();
      } else { //- otherwise when a data byte (the only data byte) is received, return NACK.      
        TWIInfo.errorCode = TWI_NO_RELEVANT_INFO;
        TWISendNACK();
      }

      break;
    
    case TWI_MR_DATA_ACK: //- data has been received, ACK has been transmitted.    
      TWIReceiveBuffer[RXBuffIndex++] = TWDR;
      if (RXBuffIndex < RXBuffLen - 0x01) { //- if there is more than one byte to be read, receive data byte and return an ACK.
        TWIInfo.errorCode = TWI_NO_RELEVANT_INFO;
        TWISendACK();
      } else { //- otherwise when a data byte (the only data byte) is received, return NACK.
        TWIInfo.errorCode = TWI_NO_RELEVANT_INFO;
        TWISendNACK();
      }

      break;
    
    case TWI_MR_DATA_NACK: //- data byte has been received, NACK has been transmitted; end of transmission.    
      TWIReceiveBuffer[RXBuffIndex++] = TWDR;	
      if (TWIInfo.repStart) { //- this transmission is complete however do not release bus yet.
        TWIInfo.errorCode = 0xFF;
        TWISendStart();
      } else { //- all transmissions are complete, exit.
        TWIInfo.mode = Ready;
        TWIInfo.errorCode = 0xFF;
        TWISendStop();
      }

      break;
    
    //- ----\/ ---- MT and MR common ----\/ ---- //
    
    case TWI_MR_SLAR_NACK: //- SLA+R transmitted, NACK received.
    case TWI_MT_SLAW_NACK: //- SLA+W transmitted, NACK received.
    case TWI_MT_DATA_NACK: //- data byte has been transmitted, NACK received.
    case TWI_LOST_ARBIT: //- arbitration has been lost.
      if (TWIInfo.repStart) { //- return error and send stop and set mode to ready.      				
        TWIInfo.errorCode = TWI_STATUS;
        TWISendStart();
      } else { //- all transmissions are complete, exit.
        TWIInfo.mode = Ready;
        TWIInfo.errorCode = TWI_STATUS;
        TWISendStop();
      }

      break;

    case TWI_REP_START_SENT: //- repeated start has been transmitted.
      TWIInfo.mode = RepeatedStartSent; //- set the mode but DO NOT clear TWINT as the next data is not yet ready.
      
      break;
    
    //- ----\/ ---- MISCELLANEOUS STATES ----\/ ----  //

    // case TWI_NO_RELEVANT_INFO: //- it is not really possible to get into this ISR on this condition.
    //   //- rather, it is there to be manually set between operations.
      
    //   break;
    
    case TWI_ILLEGAL_START_STOP: //- illegal START/STOP, abort and return error.
      TWIInfo.errorCode = TWI_ILLEGAL_START_STOP;
      TWIInfo.mode = Ready;
      TWISendStop();
      
      break;
  }
}

ISR (TWI_vect) {
  TWIStatusCheck = 0x01;
}
