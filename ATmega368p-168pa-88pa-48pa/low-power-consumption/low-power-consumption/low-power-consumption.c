/*  
    WARNING!
    BE CAREFUL WHEN DISABLING SOME FEATURES!
    FOR MORE INFORMATION, READ THE DATASHEET OF YOUR MICROCONTROLLER!
*/

#define F_CPU 16000000UL

#include "../../_avr8_include/avr/io.h"
#include "../../_avr8_include/avr/interrupt.h"

void startSleep(void);

int main(void) {
	////////////////////////////////////////////////////////////////
	//- Define all digital I/O data direction as OUTPUT,
	//- except INT0(DDD2) - INPUT, INT1(DDD3) - INPUT.
	//- DDRB - pins 8 ~ 13; DDRC - pins A0 ~ A5; DDRD - pins 0 ~ 7.
	//- DDRB/C/D - Data Direction Register B/C/D.

	DDRB = (1 << DDC6) | (1 << DDC5) | (1 << DDC4) | (1 << DDC3) | (1 << DDC2) | (1 << DDC1) | (1 << DDC0);

	DDRC = (1 << DDC5) | (1 << DDC4) | (1 << DDC3) | (1 << DDC2) | (1 << DDC1) | (1 << DDC0);

	DDRD = (1 << DDD7) | (1 << DDD6) | (1 << DDD5) | (1 << DDD4) | (1 << DDD1) | (1 << DDD0) | (DDRD & ~((1 << DDD3) | (1 << DDD2)));

	////////////////////////////////////////////////////////////////
	//- Define all digital I/O data registers as LOW.
	//- PORTB - pins 8 ~ 13; PORTC - pins A0 ~ A5; PORTD - pins 0 ~ 7.
	//- PORTB/C/D - PORT B/C/D data register.

	PORTB &= ~((1 << PORTB6) | (1 << PORTB5) | (1 << PORTB4) | (1 << PORTB3) | (1 << PORTB2) | (1 << PORTB1) | (1 << PORTB0));

	PORTC &= ~((1 << PORTC5) | (1 << PORTC4) | (1 << PORTC3) | (1 << PORTC2) | (1 << PORTC1) | (1 << PORTC0));
	
	PORTD &= ~((1 << PORTD7) | (1 << PORTD6) | (1 << PORTD5) | (1 << PORTD4) | (1 << PORTD3) | (1 << PORTD2) | (1 << PORTD1) | (1 << PORTD0));

	////////////////////////////////////////////////////////////////
	//- Disable Analog-Digital Converter.
	//- ADCSRA - Analog-Digital Converter Control and Status Register A.
	//- ADEN - Analog-Digital Converter ENable.
	//- ADIE - ADC Interrupt Enable.

	ADCSRA &= ~((1 << ADEN)); //- disable.
	// ADCSRA |= (1 << ADEN); //- enable.

	////////////////////////////////////////////////////////////////
	//- Disable Two-Wire Interface.
	//- TWCR - TWI Control Register.
	//- TWEN - TWI Enable.

	TWCR &= ~(1 << TWEN); //- disable.
	// TWCR |= (1 << TWEN)); //- enable.

	////////////////////////////////////////////////////////////////
	//- Disable Serial Peripheral Interface.
	//- SPCR - SPI Control Register 0.
	//- SPE - SPI0 Enable.

	SPCR &= ~((1 << SPE)); //- disable.
	// SPCR |= (1 << SPE)); //- enable.

	////////////////////////////////////////////////////////////////
	//- Disable Universal Synchronous Asynchronous Receiver/Transmitter.
	//- UCSR0B - USART Control and Status Register 0 B.
	//- RXEN0 - Receiver Enable 0.
	//- TXEN0 - Transmitter Enable 0.

	UCSR0B &= ~((1 << RXEN0) | (1 << TXEN0)); //- disable.
	// UCSR0B |= (1 << RXEN0) | (1 << TXEN0); //- enable.

	////////////////////////////////////////////////////////////////
  //- Shuts-down microcontroller features.
	//- PRR - Power Reduction Register.
	//- PRADC - Power Reduction ADC.
	//- PRTIM0 - Power Reduction Timer/Counter0.
	//- PRTIM1 - Power Reduction Timer/Counter1.
	//- PRTIM2 - Power Reduction Timer/Counter2.
	//- PRTWI0 - Power Reduction TWI0.
	//- PRSPI0 - Power Reduction Serial Peripheral Interface 0.
	//- PRUSART0 - Power Reduction USART0.

	PRR |= (1 << PRADC) | (1 << PRTIM0) | (1 << PRTIM1) | (1 << PRTIM2) | (1 << PRTWI) | (1 << PRSPI) | (1 << PRUSART0); //- disable.
  // PRR &= ~((1 << PRADC) | (1 << PRTIM0) | (1 << PRTIM1) | (1 << PRTIM2) | (1 << PRTWI) | (1 << PRSPI) | (1 << PRUSART0)); //- enable.

	////////////////////////////////////////////////////////////////
	//- Watchdog Timer setup.
	//- WDTCSR - Watchdog Timer Control Register.
	//- WDCE - Watchdog Change Enable (used in timed sequences for changing WDE and prescaler bits).
	//- WDE - Watchdog System Reset Enable.
	//- WDP[3:0]: Watchdog Timer Prescaler 3, 2, 1 and 0. [*1]
	//- WDIE - Watchdog Interrupt Enable.

	WDTCSR |= (1 << WDCE) | (1 << WDE);
	WDTCSR = (1 << WDP3) | (1 << WDP0);
	WDTCSR |=  (1 << WDIE); //- enable.
	// WDTCSR &=  ~(1 << WDIE); //- disable.

	////////////////////////////////////////////////////////////////
	//- External Interrupt setup.
	//- EICRA - External Interrupt Control Register A.
	//- ISC01, ISC00 - Interrupt Sense Control 0 Bit 1 and Bit 0. [*2]
	//- ISC11, ISC10 - Interrupt Sense Control 1 Bit 1 and Bit 0. [*3]
	//- EIMSK - External Interrupt Mask Register.
	//- INT0 - External Interrupt Request 0 Enable.
	//- INT1 - External Interrupt Request 1 Enable.

	EICRA |= (1 << ISC01) | (1 << ISC00);
	EIMSK |= (1 << INT0); //- enable.
	// EIMSK &= ~(1 << INT0); //- disable.

	EICRA |= (1 << ISC11) | (1 << ISC10);
	EIMSK |= (1 << INT1); //- enable.
	// EIMSK &= ~(1 << INT1); //- disable.

	////////////////////////////////////////////////////////////////
	//- Sleep mode setup.
	//- SMCR - Sleep Mode Control Register.
	//- SM[2:0] - Sleep Mode Select Bits 2, 1, and 0. [*4]

	SMCR = (1 << SM1);
	
	////////////////////////////////////////////////////////////////
  do {
		startSleep(); //- - waked up from power-down sleep mode (see [*4]) by: WDT Interrupt - after 8 seconds (see [*1]), RISING INT0 (see [*2]), RISING INT1 (see [*3]); and go back to sleep.		
	} while (0x01);
}

void startSleep(void) {
	////////////////////////////////////////////////////////////////
	//- Disable Brown-Out Detection.
	//- MCUCR - MCU Control Register.
	//- BODS - BOD Sleep.
	//- BODSE - BOD Sleep Enable.

	MCUCR |= (1 << BODS) | (1 << BODSE);
	MCUCR = (1 << BODS) | (MCUCR & ~(1 << BODSE));
	
	////////////////////////////////////////////////////////////////
	//- Enter Sleep Mode.
	//- SE - Sleep Enable. [*5]

	SMCR |= (1 << SE); //- - enable sleep.
	__asm__  __volatile__("sleep"); //- - in line assembler start sleep.
	SMCR &= ~(1 << SE); //- - disable sleep.
}

//- Run immediately after INT0 be called.
ISR(INT0_vect) {
}

//- Run immediately after INT1 be called.
ISR(INT1_vect) {
}

//- Run immediately after WDT Interrupt be called.
ISR(WDT_vect) {
}

/***

 --------------------------------------------------------------------------------------------
 [*1] Watchdog Timer Prescale Select table:

 WDP3   WDP2   WDP1   WDP0   Number of WDT Oscillator Cycles   Typical Time-out at VCC = 5.0V
 0      0      0      0      2K (2048) cycles                  16ms
 0      0      0      1      4K (4096) cycles                  32ms
 0      0      1      0      8K (8192) cycles                  64ms
 0      0      1      1      16K (16384) cycles                125ms
 0      1      0      0      32K (32768) cycles                250ms
 0      1      0      1      64K (65536) cycles                500ms
 0      1      1      0      128K (131072) cycles              1.0s
 0      1      1      1      256K (262144) cycles              2.0s
 1      0      0      0      512K (524288) cycles              4.0s
 1      0      0      1      1024K (1048576) cycles            8.0s
 --------------------------------------------------------------------------------------------
 [*2] Interrupt 0 Sense Control table:

 ISC01   ISC00   Description
 0       0       The LOW level of INT0 generates an interrupt request.
 0       1       ANY logical change on INT0 generates an interrupt request.
 1       0       The FALLING edge of INT0 generates an interrupt request.
 1       1       The RISING edge of INT0 generates an interrupt request.
 --------------------------------------------------------------------------------------------
 [*3] Interrupt 1 Sense Control table:

 ISC11   ISC10   Description
 0       0       The LOW level of INT1 generates an interrupt request.
 0       1       ANY logical change on INT1 generates an interrupt request.
 1       0       The FALLING edge of INT1 generates an interrupt request.
 1       1       The RISING edge of INT1 generates an interrupt request.
 --------------------------------------------------------------------------------------------
 [*4] Sleep Mode Select table:

 SM2   SM1   SM0   Sleep Mode
 0     0     0     Idle
 0     0     1     ADC Noise Reduction
 0     1     0     Power-down
 0     1     1     Power-save
 1     0     0     Reserved
 1     0     1     Reserved
 1     1     0     Standby
 1     1     1     External Standby
 --------------------------------------------------------------------------------------------
 [*5] To avoid the MCU entering the sleep mode unless it is the programmer's purpose, it is recommended to write the Sleep Enable (SE) bit to one just before the execution of the SLEEP instruction and to clear it immediately after waking up.
 --------------------------------------------------------------------------------------------

 ***/