#define F_CPU 1000000UL

#include "../../_avr8_include/avr/io.h"
#include "../../_avr8_include/util/delay.h"

int main(void) {
  DDRB = (1 << DDB5) | (1 << DDB4) | (1 << DDB3) | (1 << DDB2) | (1 << DDB1) | (1 << DDB0); //- set all PORTD ports as OUTPUT.
  DDRC = (1 << DDC6) | (1 << DDC5) | (1 << DDC4) | (1 << DDC3) | (1 << DDC2) | (1 << DDC1) | (1 << DDC0); //- set all PORTC ports as OUTPUT.
  DDRD = (1 << DDD7) | (1 << DDD6) | (1 << DDD5) | (1 << DDD4) | (1 << DDD3) | (1 << DDD2) | (1 << DDD1) | (1 << DDD0); //- set all PORTD ports as OUTPUT.

  uint8_t stepCount = 0x04; //- number of steps for a full stepping cycle.
  uint8_t steps[][4] = { //- the four steps of full stepping cycle.
    {0x01,0x01,0x00,0x00},
    {0x00,0x01,0x01,0x00},
    {0x00,0x00,0x01,0x01},
    {0x01,0x00,0x00,0x01}
  };

  uint16_t currentStep = 0x00; //- count every rotation cycle.
  uint16_t targetSteps = 0x800; //- 2048 single steps for a complete rotation.

  uint8_t clockwise = 0x01; //- 0x01 - clockwise; 0x00 - counterclockwise.

  while (0x01) {
    uint8_t currentStepInSequence = currentStep % stepCount; //- calculate the partial step...
    uint8_t directionStep = clockwise ? currentStepInSequence : (stepCount - 0x01) - currentStepInSequence; //- ... and define which one of the four sequences will be used.

    PORTD = 
    (steps[directionStep][0x00] << PORTD1) | //- BLUE end of the Blue/Yellow motor coil.
    (steps[directionStep][0x01] << PORTD2) | //- PINK end of the Pink/Orange motor coil.
    (steps[directionStep][0x02] << PORTD3) | //- YELLOW end of the Blue/Yellow motor coil.
    (steps[directionStep][0x03] << PORTD4);  //- ORANGE end of the Pink/Orange motor coil.

    if (++currentStep >= targetSteps) { //- after a full rotation...
      currentStep = 0x00; //- ... reset the step counting...
      clockwise = clockwise ? 0x00 : 0x01; //- ... and reverse rotation.
    }
	
	  _delay_ms(2); //- control rotation speed - 2ms is the minimum delay.
  }
}

