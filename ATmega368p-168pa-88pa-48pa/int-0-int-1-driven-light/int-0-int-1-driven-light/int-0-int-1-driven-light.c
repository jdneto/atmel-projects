#define F_CPU 16000000UL

#include "../../_avr8_include/avr/io.h"
#include "../../_avr8_include/avr/interrupt.h"

volatile uint8_t INT0Called = 0x00, INT1Called = 0x00; //- flags indicate which interrupt was called.

int main(void) {
  DDRB = (1 << DDB7) | (1 << DDB6) | (1 << DDB5) | (1 << DDB4) | (1 << DDB3) | (1 << DDB2) | (1 << DDB1) | (1 << DDB0); //- set all PORTB ports as OUTPUT.
  DDRC = (1 << DDC6) | (1 << DDC5) | (1 << DDC4) | (1 << DDC3) | (1 << DDC2) | (1 << DDC1) | (1 << DDC0); //- set all PORTC ports as OUTPUT.
  DDRD = (1 << DDD7) | (1 << DDD6) | (1 << DDD5) | (1 << DDD4) | (1 << DDD1) | (1 << DDD0); //- set all PORTD ports as OUTPUT, except PORTD3(INT0), PORTD2(INT1).

  EICRA |= (1 << ISC00); //- INT0 requested for ANY logical change in PORTD2.
  EIMSK |= (1 << INT0); //- enable INT0.

  EICRA |= (1 << ISC10); //- INT1 requested for ANY logical change in PORTD3.
  EIMSK |= (1 << INT1); //- enable INT1.

  SMCR = (1 << SM1); //- set power-down sleep mode.

  while (0x01) {
    MCUCR = (1 << BODS) | (1 << BODSE); //- prepare brown-out detection...
    MCUCR = (1 << BODS) | (MCUCR & ~(1 << BODSE)); //- ... for sleep.

    SMCR |= (1 << SE); //- enable sleep.
    __asm__  __volatile__("sleep"); //- put the microcontroller to sleep.
	    SMCR &= ~(1 << SE); //- disable sleep.

    if (INT0Called) {
      INT0Called = 0x00;

      if (PIND & (1 << PORTD2)) { //- PIND == 00000100 => PORTD2 is HIGH, closed circuit.
        PORTB &= ~(1 << PORTB1); //- set PORTB1 LOW => turn off light 0.
      } else { //- PIND2 is LOW, opened circuit.
        PORTB |= (1 << PORTB1); //- set PORTB1 HIGH => turn on light 0.
      }
    }

    if (INT1Called) {
      INT1Called = 0x00;

      if (PIND & (1 << PORTD3)) { //- PIND == 00001000 => PORTD3 is HIGH, closed circuit.
        PORTB &= ~(1 << PORTB2); //- set PORTB2 LOW => turn off light 1.
      } else { //- PIND3 is LOW, opened circuit.
        PORTB |= (1 << PORTB2); //- set PORTB2 HIGH => turn on light 1.
      }
    }
  }
}

ISR(INT0_vect) {
  INT0Called = 0x01;
}

ISR(INT1_vect) {
  INT1Called = 0x01;
}