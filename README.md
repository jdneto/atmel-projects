A collection of Atmel AVR projects.

Please, feel free to make pull requests, every help is welcome.

Files [here](https://bitbucket.org/jdneto/atmel-projects/src/).